from django.urls import path
from .import views
app_name = "accounts"
urlpatterns=[
    path('dashboard',views.dashboard,name="dashboard"),
    path('profile/<int:user_id>',views.profile,name="profile"),
    path('dashboard/bid/<int:bid_id>/delete', views.delete_bid, name="delete_bid"),
    path('profile/<int:user_id>/change_email', views.change_email, name="change_email"),
    path('profile/<int:user_id>/change_password', views.change_password, name="change_password"),
]


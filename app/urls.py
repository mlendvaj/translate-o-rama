from django.urls import path
from . import views

app_name = "app"
urlpatterns = [
    path('', views.index, name="index"),
    path('register', views.registration, name="registration"),
    path('user_login', views.custom_login, name="custom_login"),
    path('job_form', views.job_form, name="job_form"),
    path('job_form/save', views.save_job, name="save_job"),
    path('all_jobs', views.all_jobs, name="all_jobs"),
    path('all_jobs/bid/<int:job_id>', views.bid, name="bid"),
    path('all_jobs/send_message/<int:job_id>', views.send_message, name="send_message"),
    path('dashboard/<int:bid_id>/accept', views.accept_bid, name="accept_bid"),
    path('job/<int:job_id>', views.job_detail, name="job_detail"),
    path('job/<int:job_id>/dispute', views.dispute, name="dispute"),
    path('job/<int:job_id>/messenger', views.job_messenger, name="job_messenger"),
    path('dashboard/job/<int:job_id>/delete', views.delete_job, name="delete_job"),
    path('job/<int:job_id>/translate', views.translate, name="translate"),
    path('job/<int:job_id>/review', views.review, name="review"),
    path('job/<int:job_id>/review/rate', views.rate, name="rate"),
    path('messenger',views.messenger, name="messenger"),
]

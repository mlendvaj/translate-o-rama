from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Profile,Job,Bid,Dispute,Message,Rating

# Register your models here.
#admin.site.register(Profile, UserAdmin)

admin.site.register(Profile)
admin.site.register(Job)
admin.site.register(Bid)
admin.site.register(Dispute)
admin.site.register(Message)
admin.site.register(Rating)

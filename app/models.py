from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=100,blank=True)
    balance = models.DecimalField(max_digits=8,decimal_places=2,default=0)
    translator=models.BooleanField(default=False)
    def __str__(self):
        return f"{self.id} - {self.name}"

    def is_translator(self):
        return self.translator

    def active_jobs(self):
        return self.user.job_set.exclude(status='completed')


class Job(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    source_lang = models.CharField(max_length=100)
    target_lang = models.CharField(max_length=100)
    field = models.CharField(max_length=100)
    budget = models.DecimalField(max_digits=8,decimal_places=2)
    text = models.TextField()
    status = models.CharField(max_length=100,default='available')
    translation = models.TextField(blank=True,null=True)
    def __str__(self):
        return f"{self.user.profile.name} - {self.title}"

    def accepted_bid(self):
        return self.bid_set.filter(accepted=True).first()

    def accepted_bidder(self):
        return self.accepted_bid().bidder.profile

    def has_dispute(self):
        dispute = Dispute.objects.filter(job_id=self.id)
        if dispute is None:
            return False
        else:
            return True

    @classmethod
    def accepted_jobs_for(cls,user):
        #Svi bidovi koje je user napravio job na koji je bidano
        accepted_bids = Bid.objects.filter(job__in=user.job_set.all(),accepted=True)
        #Svi jobovi gdje je user napravio job i bidano na job
        accepted_jobs = [ bid.job for bid in accepted_bids if bid.job.status != "completed"]
        return accepted_jobs

class Bid(models.Model):
    bidder = models.ForeignKey(User,on_delete=models.CASCADE)
    job = models.ForeignKey(Job,on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=8,decimal_places=2)
    accepted = models.BooleanField()
    def __str__(self):
        return f"{self.bidder.profile.name} - {self.job.title} - {self.price}"

    def user_bids(self):
        return self.objects.all().filter(bidder=user)


class Message(models.Model):
    sender = models.ForeignKey(User,on_delete=models.CASCADE,related_name='sender')
    reciever = models.ForeignKey(User,on_delete=models.CASCADE,related_name='reciever')
    text = models.TextField()
    job = models.ForeignKey(Job, on_delete=models.CASCADE)

class Rating(models.Model):
    rater = models.ForeignKey(User,on_delete=models.CASCADE,related_name='rater')
    rated = models.ForeignKey(User,on_delete=models.CASCADE,related_name='rated')
    job = models.ForeignKey(Job,on_delete=models.CASCADE)
    rating = models.IntegerField()


class Dispute(models.Model):
    job = models.OneToOneField(Job, on_delete=models.CASCADE)
    reason = models.TextField()

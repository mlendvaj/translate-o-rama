from django.contrib.auth import authenticate, login
from django.shortcuts import render, get_object_or_404
from .models import Profile
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from .models import Job, Bid, Message, Rating, Dispute, Profile


# Create your views here.

def home(request):
    context = {}
    return render(request, 'app/home.html', context)

def index(request):
    return render(request, 'app/home.html', context={})

def registration(request):
    if request.method == 'GET':
        return render(request, 'registration/registration.html', context={})
    elif request.method == 'POST':
        first_name = request.POST["name"]
        last_name = request.POST["last_name"]
        email = request.POST["email"]
        password = request.POST["password"]
        username = email.split("@")[0]

        user = User.objects.create_user(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password,
        )

        translator = request.POST.getlist('is_translator')
        if translator == ['translator']:
            translator = True
        else:
            translator = False

        profile = Profile(
            user=user,
            name=first_name + " " + last_name,
            balance=0,
            translator=translator
        )
        profile.save()
        return HttpResponseRedirect(reverse("login", args=[]))


def custom_login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=email.split("@")[0], password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("app:index", args=[]))
        else:
            messages.error(request, 'Invalid creditentials')
            return HttpResponseRedirect(reverse("app:custom_login", args=[]))
    elif request.method == 'GET':
        return render(request, 'registration/login.html', context={})
    else:
        return HttpResponseRedirect(reverse("app:index", args=[]))
        
@login_required
def job_form(request):
    context = {}
    return render(request, 'app/post_job.html', context)

@login_required
def save_job(request):
    job = Job(
        user = request.user,
        title = request.POST["title"],
        description = request.POST["description"],
        source_lang = request.POST["source_language"],
        target_lang = request.POST["target_language"],
        field = request.POST["job_field"],
        budget = request.POST["budget"],
        text = request.POST["translate_text"],
        translation = "",
    )
    job.save()
    return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))

@login_required
def all_jobs(request):
    jobs = Job.objects.filter(status='available')
    context ={
        'jobs' : jobs,
    }
    return render(request, 'app/jobs.html', context)

@login_required
def bid(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    bid = Bid(
        bidder = request.user,
        job = job,
        price = request.POST["price"],
        accepted = False,
    )
    bid.save()
    return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))

@login_required
def send_message(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    message = Message(
        sender = request.user,
        reciever = job.user,
        text = request.POST["message"],
        job = job,
    )
    message.save()
    return HttpResponseRedirect(reverse("app:job_messenger", args=[job.id]))


@login_required
def accept_bid(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job

    if job.user == request.user:
        bid.accepted = True
        bid.save()
        job.status = "assigned"
        job.save()
    return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))

@login_required
def job_detail(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    messages = job.message_set.all()

    visitor = request.user
    translator = job.accepted_bidder()

    context = {
        'messages' : messages,
        'job': job,
        'visitor': visitor,
        'translator':translator,
    }
    #return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))
    return render(request, 'app/job_detail.html', context)

@login_required
def job_messenger(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    messages = job.message_set.all()

    my_jobs = request.user.job_set.all()
    assigned_jobs = request.user.bid_set.all().filter(accepted=True)
    my_messages = Message.objects.filter(sender=request.user)

    context = {
        'messages' : messages,
        'job': job,
        'my_jobs':my_jobs,
        'assigned_jobs':assigned_jobs,
        'my_messages':my_messages,
    }
    #return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))
    return render(request, 'app/job_messenger.html', context)

@login_required
def delete_job(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    job.delete()
    return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))

@login_required
def translate(request, job_id):
    job = get_object_or_404(Job,pk=job_id)
    translation = request.POST["translate_text"]
    if translation != "":
        job.translation = translation
        job.status = "completed"
        job.save()
    return HttpResponseRedirect(reverse("app:job_detail", args=[job.id]))

@login_required
def review(request, job_id):
    job = get_object_or_404(Job,pk=job_id)
    translator = job.bid_set.all().filter(accepted=True).first().bidder
    visitor = request.user

    context = {
        'job':job,
        'translator':translator,
        'visitor':visitor,
    }
    return render(request, 'app/review.html', context)

@login_required
def rate(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    translator = job.bid_set.all().filter(job=job, accepted=True).first().bidder

    bid = translator.bid_set.all().filter(job=job, accepted=True).first()

    if request.user.profile.balance > translator.profile.balance:
        translator.profile.balance = translator.profile.balance + bid.price
        translator.profile.save()

        request.user.profile.balance = request.user.profile.balance - bid.price
        request.user.profile.save()

        rating = Rating(
            rater = request.user,
            rated = translator,
            job=job,
            rating=request.POST["inlineRadioOptions"],
        )
        rating.save()

    return HttpResponseRedirect(reverse("accounts:profile", args=[request.user.id]))
    #dodati rating iz forme
@login_required
def dispute(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    context = {
        'job' : job,
    }
    if request.method == "GET":
        return render(request, 'app/dispute.html', context)
    if request.method == "POST":
        reason = request.POST["reason"]
        dispute = Dispute.objects.create(
            job = job,
            reason= reason,
        )
        dispute.save()
        return HttpResponseRedirect(reverse("app:index", args=[]))


@login_required
def messenger(request):
    user = request.user
    my_jobs = user.job_set.all()
    assigned_jobs = user.bid_set.all().filter(accepted=True)

    my_messages = Message.objects.filter(sender=user)

    context={
        'my_jobs':my_jobs,
        'assigned_jobs':assigned_jobs,
        'my_messages':my_messages,
    }
    return render(request, 'app/messenger.html', context)

from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from app.models import Profile,Job,Dispute,Message,Rating,Bid
from django.contrib.auth.models import User



# Create your views here.
@login_required
def dashboard(request):
    user = request.user
    my_jobs = user.job_set.all()
    my_bids = user.bid_set.all()
    accepted_jobs = user.profile.active_jobs()
    #completed_jobs = user.job_set.all().filter(status="completed")

    #assigned_jobs = user.bid_set().all().filter(bidder=user, accepted=True)

    won = user.bid_set.filter(accepted=True)

    rating_list = Rating.objects.all().filter(rated=user)
    sum_rating=0
    if len(rating_list) == 0:
        average=0
    else:
        for rating in rating_list:
            sum_rating=sum_rating+rating.rating
        average = sum_rating/len(rating_list)
        
    #my_accepted_bids = user.bid_set.all().filter(accepted=True)
    context = {
        'my_jobs': my_jobs,
        'my_bids': my_bids,
        #'my_accepted_bids': my_accepted_bids,
        #'accepted_jobs': accepted_jobs,
        #'completed_jobs': completed_jobs,
        'average': average,
        'won':won,
        #'assigned_jobs':assigned_jobs,
        
    }
    return render(request,'accounts/dashboard.html',context)

@login_required
def profile(request,user_id):
    user = get_object_or_404(User,pk=user_id)
    visitor = request.user

    accepted_jobs = Job.accepted_jobs_for(user)
    #completed_jobs = Job.objects.filter(user=user,status='completed')

    my_jobs = user.job_set.all()
    my_bids = user.bid_set.all()

    won = user.bid_set.filter(accepted=True)

    
    rating_list = Rating.objects.all().filter(rated=user)
    sum_rating=0
    if len(rating_list) == 0:
        average=0
    else:
        for rating in rating_list:
            sum_rating=sum_rating+rating.rating
        average = (sum_rating/len(rating_list))

    context = {
        'profile_user': user,
        'accepted_jobs': accepted_jobs,
        #'completed_jobs': completed_jobs,
        'average':average,
        'my_jobs': my_jobs,
        'my_bids': my_bids,
        'visitor': visitor,
        'won':won,
    }
    return render(request,'accounts/profile.html',context)

@login_required
def delete_bid(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    bid.delete()
    return HttpResponseRedirect(reverse("accounts:dashboard", args=[]))

@login_required
def change_email(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if request.POST["email"] != "":
        user.email = request.POST["email"]
        user.save()
    return HttpResponseRedirect(reverse("accounts:profile", args=[user.id]))

@login_required
def change_password(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if request.POST["password"] != "":
        user.email = request.POST["password"]
        user.save()
    return HttpResponseRedirect(reverse("accounts:profile", args=[user.id]))

